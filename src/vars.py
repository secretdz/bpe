#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from src.funcs import mangleFunctionName


def showRawAddr(self, name, addr):
    self.exe.printRawAddr(name, addr)


def showVaAddr(self, name, addr):
    self.exe.log("{0}: {1}".format(name, hex(addr)))


def addVaCommentAddr(self, name, addr, pos):
    self.addrList.append(
        ("ExtLine", name, hex(addr), pos))


def addVaFunctionCommentAddr(self, name, addr):
    self.addrList.append(
        ("SetFunctionCmt", name, hex(addr), 1))


def addRawFunc(self, name, addr):
    addr = self.exe.rawToVa(addr)
    addVaFunc(self, name, addr)


def addVaFunc(self, name, addr, show=True):
    if show is True:
        self.exe.log("{0}: {1}".format(name, hex(addr)))
    name1 = mangleFunctionName(name)
    if name != name1:
        addVaFunctionCommentAddr(self, name, addr)
    val = ("MakeName", name1, hex(addr))
    self.addrList.append(val)
    if addr in self.addrNameDict:
        if val != self.addrNameDict[addr]:
            self.exe.log("Error: found duplicate function {0} {1}"
                         .format(self.addrNameDict[addr], val))
            exit(1)
        else:
            # adding same object
            return
    self.addrNameDict[addr] = val


def addRawVar(self, name, addr):
    addr = self.exe.rawToVa(addr)
    addVaVar(self, name, addr)


def addVaVar(self, name, addr, show=True):
    if show is True:
        self.exe.log("{0}: {1}".format(name, hex(addr)))
    name1 = mangleFunctionName(name)
    if name != name1:
        addVaCommentAddr(self, name, addr, 0)
    val = ("MakeName", name1, hex(addr))
    self.addrList.append(val)
    self.addrNameDict[addr] = val


def addRawArray(self, name, addr, size, addFlag):
    addr = self.exe.rawToVa(addr)
    self.addVaArray(name, addr, size, addFlag)


def makeUnknown(self, addr, size):
    val = ("MakeUnknown", "", hex(addr), hex(size), "DOUNK_SIMPLE")
    self.addrList.append(val)


def makeDword(self, addr):
    val = ("MakeDword", "", hex(addr))
    self.addrList.append(val)


def makeByte(self, addr):
    val = ("MakeByte", "", hex(addr))
    self.addrList.append(val)


def addVaArray(self, name, addr, size, addFlag):
    self.exe.log("{0}[{2}]: {1}".format(name, hex(addr), size))
    if addFlag is True:
        makeUnknown(self, addr, size)
        makeByte(self, addr)
        self.addrList.append(("MakeArray", "", hex(addr), hex(size)))
        val = ("MakeName", name, hex(addr))
        self.addrList.append(val)
        self.addrNameDict[addr] = val


def getStructIdByName(self, name):
    self.currentStructName = name
    val = ("GetStrucIdByName", name, 0)
    self.addrList.append(val)


def addStruct(self, name):
    if name in self.strcutsDefined:
        getStructIdByName(self, name)
        return
    self.currentStructName = name
    val = ("AddStrucEx", name, -1)
    self.addrList.append(val)
    self.strcutsDefined.add(name)


def showStructMember(self, name, offset):
    self.exe.log("{0}::{1}: {2}".format(self.currentStructName,
                                        name,
                                        hex(offset)))


def addStructMember(self, name, offset, size, show=False):
    if show is True:
        showStructMember(self, name, offset)
    if size == 1:
        typeStr = "FF_BYTE | FF_DATA"
    elif size == 2:
        typeStr = "FF_WORD | FF_DATA"
    elif size == 4:
        typeStr = "FF_DWRD | FF_DATA"
    elif size == 8:
        typeStr = "FF_QWRD | FF_DATA"
    else:
        typeStr = "FF_DATA"
    # add member to struct selected by addStruct or getStrucIdByName
    val = ("AddStrucMember",
           name,
           0,
           "id",
           hex(offset),
           typeStr,
           -1,
           size)
    self.addrList.append(val)


def setStructMemberType(self, offset, typeStr):
    # set member type to struct selected by addStruct or getStrucIdByName
    val = ("SetType", "", 0, "id", offset, typeStr)
    self.addrList.append(val)


def setStructComment(self, comment):
    # set comment to struct selected by addStruct or getStrucIdByName
    val = ("SetStrucComment", "", 0, "id", comment, 1)
    self.addrList.append(val)


def setStructMemberComment(self, offset, comment):
    # set comment to struct member in struct selected by
    # addStruct or getStrucIdByName
    val = ("SetMemberComment", "", 0, "id", offset, comment, 1)
    self.addrList.append(val)
