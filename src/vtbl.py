#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re


ampersantSplit = re.compile("@")


class ClassVtbl:
    def __init__(self, name, offsetR):
        self.name = name
        self.offsetR = offsetR
        self.baseDescriptors = []


def readPointersArray(self, arrayPointer, maxCnt, rdataSection):
    pointers = []
    cnt = 0
    readUInt = self.exe.readUInt
    data = readUInt(arrayPointer)
    while data != 0 and cnt < maxCnt:
        pointers.append(rdataSection.vaToRaw(data))
        arrayPointer = arrayPointer + 4
        cnt = cnt + 1
        data = readUInt(arrayPointer)
    if data != 0:
        return []
    return pointers


def readVtbl(readUInt, pointer, textRange, textSection):
    vtbl = []
    data = readUInt(pointer)
    while data != 0 and textRange.isIn(data):
        data2 = readUInt(textSection.vaToRaw(data))
        if data2 == 0:
            break
        vtbl.append(data)
        pointer = pointer + 4
        data = readUInt(pointer)
    return vtbl


def isClassNameValid(className):
    return className is not None and \
        len(className) >= 5 and className[:2] == ".?"


# demangling based on http://www.kegel.com/mangle.html
def demangleClassName(name):
    # remove ".?" at start and "@" at end
    name = name[2:-1]
    storage = name[0]
    dataType = name[1]
    name = name[2:]
    isClass = True
    if storage == "A":
        # __cdecl
        pass
    else:
        return None
    if dataType == "V":
        isClass = True
    elif dataType == "U":
        # struct
        isClass = False
    else:
        return None
    parts = ampersantSplit.split(name)
    parts2 = []
    idx = 0
    while idx < len(parts):
        part = parts[idx]
        if len(part) > 2:
            if part[:2] == "?A":
                part = "anonymous"
            elif part[:2] == "?$":
                idx = idx + 1
                part2 = parts[idx][1:]
                part = "{0}<{1}>".format(part[2:], part2)
        if part != "":
            parts2.append(part)
        idx = idx + 1
    name = ""
    for part in reversed(parts2):
        if name == "":
            name = part
        else:
            name = name + "::" + part
    return (name, (isClass,))


def debugPrint(obj, rdataSection):
    baseStr = ""
    for ptr in obj.baseClasseDescriptors:
        if baseStr != "":
            baseStr = baseStr + ","
        baseStr = baseStr + hex(rdataSection.rawToVa(ptr))
    membersStr = ""
    for ptr in obj.members:
        if membersStr != "":
            membersStr = membersStr + ","
        membersStr = membersStr + hex(ptr)
    classStr = ""
    for name in obj.mangledBaseClassNames:
        if classStr != "":
            classStr = classStr + ","
        name2, _ = demangleClassName(name)
        classStr = "{0}{1} ({2})".format(classStr, name, name2)
    print(("Class {0} ({1}): {2}\n base pointers: {3}\n "
           "base classes: {4}\n members: {5}").
          format(obj.mangledClassName,
          obj.name,
          hex(rdataSection.rawToVa(obj.offsetR)),
          baseStr,
          classStr,
          membersStr))


def getBaseClassesNameStr(obj):
    classStr = ""
    for name in obj.mangledBaseClassNames:
        if classStr != "":
            classStr = classStr + ", "
        name2, _ = demangleClassName(name)
        if name2 == obj.name:
            continue
        # skip base classes with complex mangling
        if name2.find("?$char_traits") > 0:
            continue
        offsets = obj.mangledBaseClassOffsets[name]
        if offsets != (0, -1, 0):
            classStr = "{0}{1} {2}".format(classStr,
                                           name2,
                                           offsets)
        else:
            classStr = "{0}{1}".format(classStr,
                                       name2)
    return classStr
