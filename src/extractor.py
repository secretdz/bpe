#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from src.asm import Asm
from src.exe import Exe
from src.funcs import toUtf8
from src.math import Math

import os
import re

packetVersionRe = re.compile(
    "^(?P<v1>[\d][\d][\d][\d])-(?P<v2>[\d][\d])-(?P<v3>[\d][\d])")


class Extractor:
    from src.extractors.basicinfo import getBasicInfo
    from src.extractors.charinfo import searchCharInfo
    from src.extractors.combo import searchCombo
    from src.extractors.cheatdefender import searchCheatDefender
    from src.extractors.connectionstartup import searchConnectionStartup
    from src.extractors.errormsg import searchErrorMsg
    from src.extractors.getgamemode import searchGetGameMode
    from src.extractors.hook import searchHookAddr
    from src.extractors.loginpacket import searchLoginPacketHandler
    from src.extractors.mappacket import searchMapPacketHandler
    from src.extractors.maxchars import searchMaxChars
    from src.extractors.modemgr import searchModeMgr
    from src.extractors.msgstringtable import searchMsgStringTable, \
        extractMsgStrings
    from src.extractors.oep import searchOEP
    from src.extractors.packetbuf import searchPacketBuf
    from src.extractors.recvpacket import searchRecvPacket
    from src.extractors.recvpackethooks import searchRecvPacketHooks
    from src.extractors.sendencryption import searchSendEncryption
    from src.extractors.sendpacketold import searchSendPacketOld
    from src.extractors.sendpacketold2 import searchSendPacketOld2
    from src.extractors.servertype import searchServerType
    from src.extractors.servicetype import searchServiceType
    from src.extractors.sendpacket import searchSendPacket
    from src.extractors.session import searchSession
    from src.extractors.shuffle import searchShuffle
    from src.extractors.vtbl import searchVtbl, vtblNameMembers, vtblAddStruct
    from src.extractors.windowmgr import searchWindowMgr
    from src.vars import showRawAddr, showVaAddr, addVaCommentAddr, \
        addRawFunc, addVaFunc, addRawVar, addVaVar, addRawArray, addVaArray, \
        addStruct, addStructMember, makeUnknown, makeDword, \
        setStructMemberType, setStructComment, setStructMemberComment

    def __init__(self):
        self.offset1 = None
        self.asmEndPos = 0
        self.hookAddr = 0
        self.hookAddr2 = 0
        self.windowFunc = 0
        # list of addr objects (names, comments, etc)
        self.addrList = []
        # dict of addr objects names. addr=addrObj
        self.addrNameDict = dict()
        self.strcutsDefined = set()
        self.ollyScript = ""
        self.loginPollAddrVa = 0
        self.gamePollAddrVa = 0
        self.connectionStartup = 0
        self.connectionStartupVa = 0
        self.sendPacket = 0
        self.sendAddr = 0
        self.recvAddr = 0
        self.packetParser1BytesCount = 16
        self.packetParser2BytesCount = 16
        self.packetParser3BytesCount = 16
        self.packetParser4BytesCount = 16
        self.instanceR = 0
        self.cleanFileName = ""
        self.gCheatDefenderMgr = 0
        self.cheatDefenderMgrInit = 0
        self.packetBuf = 0
        self.hookRecvPacketStartMap = 0
        self.hookRecvPacketExitMap = 0
        self.callRecvPacket1 = 0
        self.g_modeMgr = 0
        self.CMode_mWorldOffset = 0
        self.getPacketSizeFunction = 0
        self.vtables = None
        self.comboSimple = False


    def close(self):
        self.exe.close()
        self.vtables = None
        self.addrNameDict = None
        self.strcutsDefined = set()
        self.msgStrings = None


    def getAddr(self, offset, addrOffset, instructionOffset):
        ptr = self.exe.readUInt(offset + addrOffset)
        return Math.sumOffset(offset + instructionOffset, ptr)


    def walkUntilRet(self):
        asm = Asm()
        asm.exe = self.exe
        asm.init(self.initPacketLenWithClientFunction)
        # asm.printLine()
        while asm.move() is True:
            # asm.printLine()
            pass
        self.asmEndPos = asm.pos
        self.showRawAddr("Last asm walk addr", self.asmEndPos)
        print("Asm code walked")


    def runAsmSimple(self):
        asm = Asm()
        asm.debug = False
        asm.debugMemory = False
        asm.exe = self.exe
        asm.init(self.initPacketLenWithClientFunction)
        asm.initState(0)
        # asm.printRegisters()
        # asm.printLine()
        asm.run()
        # asm.printRegisters()
        while asm.move() is True:
            # asm.printLine()
            asm.run()
            # asm.printRegisters()
            pass
        self.stackAlign = asm.popCounter
        self.callFunctions = asm.callFunctions
        self.allCallFunctions = asm.callFunctions
        self.allCallAddrs = asm.allCallAddrs
        print("Asm simple code complete")
        # print(asm.memory)


    def runAsmCollect(self):
        asm = Asm()
        asm.debug = False
        asm.debugMemory = False
        asm.stackAlign = self.stackAlign
        asm.exe = self.exe
        asm.init(self.initPacketLenWithClientFunction)
        asm.initState(1)
        asm.callFunctions = self.callFunctions
        asm.allCallFunctions = self.allCallFunctions
        # asm.printRegisters()
        # asm.printMemory()
        # asm.printLine()
        asm.run()
        while asm.move() is True:
            asm.run()
            pass
        self.packets = asm.packets
        self.callFunctions = asm.callFunctions
        print("Asm collector code complete")


    def parseCallStack(self):
        correctFunctions = dict()
        for addr in self.callFunctions:
            callFunction = self.callFunctions[addr]
            if callFunction.callCounter < 10:
                continue
            correctCounts = dict()
            tmpPushes = 0
            # walk all push counts and remove used very rare
            for pushCount in callFunction.counts:
                num = callFunction.counts[pushCount]
                if num > 10:
                    correctCounts[pushCount] = num
                    tmpPushes = pushCount
            callFunction.counts = correctCounts
            # need at least two different function calls
            # in initPacketMapWithClient
            if len(callFunction.counts) > 1:
                self.exe.log("Error. Different number of pushes before " +
                             "function call: {0}\n{1}".
                             format(self.exe.getAddrSec(addr),
                                    callFunction.counts))
                exit(1)
            callFunction.adjustSp = tmpPushes
            # print("addr {0}, pushes {1}".format(hex(self.exe.rawToVa(addr)),
            #                                     callFunction.adjustSp))
            correctFunctions[addr] = callFunction
        self.callFunctions = correctFunctions


    def checkPackets(self):
        error = False
        for key in self.callFunctions:
            func = self.callFunctions[key]
            if len(func.collectCallsFrom) < 1:
                continue
            for calladdr in func.callsFrom:
                if calladdr not in func.collectCallsFrom:
                    print("Error function was not called from addr: {0}".
                          format(self.exe.getAddrSec(calladdr)))
                    error = True
        if error is True:
            exit(1)


        if len(self.packets) < 380:
            self.exe.log("Error. too small packets amount: {0}".format(
                len(self.packets)))
            exit(1)


    def savePackets(self):
        self.exe.log("Packets number: {0}".format(len(self.packets)))
        processedKeys = dict()
        with open("{0}/bpe_data_{1}.ini".format(self.exe.outDir,
                                                self.exe.client_date
                                                ), "wt") as w:
            for packet in self.packets:
                packetId = hex(packet[0]).upper()
                if packet[0] in processedKeys:
                    newLen = processedKeys[packet[0]][1]
                    if newLen == packet[1]:
                        continue
                    self.exe.log("Warning. Duplicate packet with " +
                                 "different size: {0}: {1} vs {2}".
                                 format(hex(packet[0]),
                                        packet[1],
                                        newLen
                                        ))
                while len(packetId) < 6:
                    packetId = packetId[:2] + "0" + packetId[2:]
                packetId = packetId[0] + "x" + packetId[2:]
                w.write("{0},{1},{2},{3}\n".format(packetId,
                                                   packet[1],
                                                   packet[2],
                                                   packet[3]))
                processedKeys[packet[0]] = packet
        processedKeys = dict()
        cleanPackets = dict()
        for packet in self.packets:
            cleanPackets[packet[0]] = packet
        with open("{0}/bpe_PacketLengths_{1}.ini".format(self.exe.outDir,
                                                         self.exe.client_date
                                                         ), "wt") as w:
            w.write("[Packet_Lengths]\r\n")
            for packet in self.packets:
                packet = cleanPackets[packet[0]]
                packetId = hex(packet[0]).upper()
                if packet[0] in processedKeys:
                    newLen = processedKeys[packet[0]][1]
                    if newLen == packet[1]:
                        continue
                    self.exe.log("Warning. Duplicate packet with different " +
                                 "size: {0}: {1} vs {2}".
                                 format(hex(packet[0]),
                                        packet[1],
                                        newLen))
                while len(packetId) < 6:
                    packetId = packetId[:2] + "0" + packetId[2:]
                packetId = packetId[0] + "x" + packetId[2:]
                w.write("{0} = {1}\r\n".format(packetId, packet[1]))
                processedKeys[packet[0]] = packet
            w.write("[Shuffle_Packets]\r\n")


    def getHexAddr(self, addr):
        addr = hex(addr)[2:]
        return "{0}{1}".format("0" * (8 - len(addr)), addr)


    def getSimpleAddr(self, addr):
        addr = hex(self.exe.rawToVa(addr))[2:]
        return "{0}{1}".format("0" * (8 - len(addr)), addr)


    def addScriptCommand(self, w, cmd):
        w.write(cmd + "\n")
        self.ollyScript = self.ollyScript + cmd + "\n"
        print(cmd)


    def addScriptCommand2(self, w, cmd):
        w.write(cmd + "\n")
        print(cmd)


    def cleanupName(self, fileName):
        if fileName[-2:] == "_D":
            fileName = fileName[:-2]
        if fileName[-5:] == "_dump":
            fileName = fileName[:-5]
        if fileName[-3:] == "_DP":
            fileName = fileName[:-3]
        self.cleanFileName = fileName


    def saveClasses(self):
        self.cleanupName(self.exe.fileName)
        outDir = "output/classes/"
        if not os.path.exists(outDir):
            os.makedirs(outDir)
        names = []
        for obj in self.vtables:
            offsets = []
            if obj.vtblOffset != 0:
                offsets.append("vtblOffset={0}".format(obj.vtblOffset))
            if obj.constructorOffset != 0:
                offsets.append("constructorOffset={0}".
                               format(obj.constructorOffset))
            if obj.attr != 0:
                offsets.append("attr={0}".format(obj.attr))
            offsetStr = ",".join(offsets)
            if offsetStr != "":
                offsetStr = " (" + offsetStr + ")"
            formatStr = "{name} {type}{extra}: " + \
                        "{offset},{members} {base}\n"
            names.append(formatStr.format(name=obj.name,
                                          type=obj.typeStr,
                                          offset=hex(obj.offsetV + 4),
                                          members=len(obj.members),
                                          base=obj.baseClassesStr,
                                          extra=offsetStr))
        with open("{0}{1}.txt".format(outDir, self.cleanFileName), "wt") as w:
            for name in sorted(names):
                w.write(name)
        self.exe.log("Saved {0} classes".format(len(self.vtables)))


    def saveMsgString(self):
        self.cleanupName(self.exe.fileName)
        outDir = "output/msgstringtable/" + self.cleanFileName
        if not os.path.exists(outDir):
            os.makedirs(outDir)
        separator = "-" * 80
        with open("{0}/msgstringtable_text.txt".format(outDir), "w") as wt:
            with open("{0}/msgstringtable_hex.txt".format(outDir), "w") as wh:
                with open("{0}/msgstringtable.txt".format(outDir), "w") as wm:
                    for key in self.msgStrings:
                        msg = self.msgStrings[key]
                        wt.write("{0} ({1}):\n{2}\n{3}\n".format(key,
                                                                 hex(key),
                                                                 toUtf8(msg),
                                                                 separator))
                        wh.write("{0}: {1}\n".format(hex(key),
                                                     msg.encode("hex")))
                        msg = msg.replace("#", "_")
                        msg = msg.replace("\n", "\\n")
                        msg = msg.replace("\r", "\\r")
                        wm.write("{0}#\n".format(msg))


    def init(self, fileName, outFileName):
        exe = Exe()
        if exe.load(fileName, outFileName) is False:
            print("Skipping file {0}".format(fileName))
            exit(1)
        m = packetVersionRe.search(fileName)
        if m is not None:
            self.packetVersion = m.group("v1") + m.group("v2") + m.group("v3")
        else:
            self.packetVersion = "unknown"
        self.exe = exe


    def makeOutDir(self):
        if not os.path.exists("output"):
            os.makedirs("output")


    def searchBasicInfo(self):
        parsed = self.getBasicInfo()
        if parsed is False:
            self.exe.log("Error: basic parsing info failed")
            exit(1)


    def getpackets(self):
        self.makeOutDir()
        self.searchBasicInfo()
        self.walkUntilRet()
        self.runAsmSimple()
        self.parseCallStack()
        self.runAsmCollect()
        self.checkPackets()
        self.savePackets()


    def getInfo(self):
        self.makeOutDir()
        self.searchVtbl(True)
        self.vtblNameMembers()
        self.searchBasicInfo()
        self.searchLoginPacketHandler()
        self.searchMapPacketHandler()
        self.searchSendPacketOld(True)
        self.searchSendPacketOld2(True)
        self.searchSendPacket(1)
        self.searchPacketBuf(False)
        self.searchCharInfo(False)
        self.searchRecvPacket(False, False)
        self.searchCombo()
        self.searchOEP()
        self.searchShuffle(False)
        self.searchCheatDefender(False)
        self.searchSendEncryption(True)
        self.searchWindowMgr(False)
        self.searchServiceType(False)
        self.searchServerType(False)
        self.searchMsgStringTable()
        self.searchConnectionStartup(False)
        self.searchErrorMsg(True)
        self.searchGetGameMode()
        self.searchModeMgr()
        self.searchSession(False)
        self.searchMaxChars(False)
        self.vtblAddStruct()


    def getOlly(self):
        self.makeOutDir()
        self.searchBasicInfo()
        self.walkUntilRet()
        self.runAsmSimple()
        self.parseCallStack()
        self.searchLoginPacketHandler()
        self.searchMapPacketHandler()
        self.searchSendPacketOld(True)
        self.searchSendPacketOld2(True)
        self.searchSendPacket(1)
        self.searchCombo()
        self.searchHookAddr()


    def getIda(self):
        self.makeOutDir()
        self.searchBasicInfo()
        self.searchLoginPacketHandler()
        self.searchMapPacketHandler()


    def getPacketLogger(self):
        self.makeOutDir()
        if self.searchCheatDefender(True):
            self.searchBasicInfo()
            self.searchConnectionStartup(True)
            self.searchLoginPacketHandler()
            self.searchMapPacketHandler()
            self.searchSendPacket(True)
            self.searchRecvPacket(True, True)
            self.searchRecvPacketHooks()
            self.searchSendEncryption(True)
        else:
            self.searchConnectionStartup(True)


    def getMaxChars(self):
        self.makeOutDir()
        self.searchMaxChars(True)


    def getClass(self):
        self.makeOutDir()
        self.searchVtbl(True)
        self.saveClasses()


    def getMsgStringTable(self):
        self.makeOutDir()
        self.searchServiceType(True)
        self.searchMsgStringTable()
        self.extractMsgStrings()
        self.saveMsgString()


    def getDebug(self):
        self.makeOutDir()
        self.searchServiceType(True)
        self.searchMsgStringTable()
