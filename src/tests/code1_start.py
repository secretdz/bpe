#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

from src.tests.common import loadClient


class code1_start(unittest.TestCase):
    def setUp(self):
        self.extractor = loadClient("head.exe")


    def tearDown(self):
        self.extractor = None


    def test1(self):
        code = ("MZ\x90\x00\x03\x00\x00\x00")
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)


    def test2(self):
        code = ("MZ\x90\x00\x03\x00\x01\x00")
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is False)


    def test3(self):
        code = ("MZ\x90\x00\x03\x00\x00\x00")
        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)


    def test4(self):
        code = ("MZ\x90\xAB\x03\xAB\xAB\x00")
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)


    def test5(self):
        code = ("MZ\x90\xAB\x03\xAB\xAB\x00")
        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is False)
