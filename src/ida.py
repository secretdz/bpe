#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from src.templates import Templates


class Ida:
    def addLine(self, w, text):
        w.write(text + "\n")


    def getScript(self, extractors):
        fileName = "output/decompile_{0}.sh".format(
            os.path.basename(os.path.abspath(".")))
        with open(fileName, "wt") as w:
            w.write("#!/bin/bash\n")
            for extractor in extractors:
                w.write("run \"{0}\" \"{1}\" \"{2}\"\n".format(
                    extractor.exe.fileName,
                    hex(extractor.loginPollAddrVa),
                    hex(extractor.gamePollAddrVa)))


    @staticmethod
    def saveIdaScript(extractors):
        template = Templates("file.idc", "")
        MakeName = Templates.loadTemplateFile("MakeName.idc")
        MakeByte = Templates.loadTemplateFile("MakeByte.idc")
        MakeDword = Templates.loadTemplateFile("MakeDword.idc")
        MakeArray = Templates.loadTemplateFile("MakeArray.idc")
        MakeUnknown = Templates.loadTemplateFile("MakeUnknown.idc")
        ExtLine = Templates.loadTemplateFile("ExtLine.idc")
        SetFunctionCmt = Templates.loadTemplateFile("SetFunctionCmt.idc")
        AddStrucEx = Templates.loadTemplateFile("AddStrucEx.idc")
        GetStrucIdByName = Templates.loadTemplateFile("GetStrucIdByName.idc")
        AddStrucMember = Templates.loadTemplateFile("AddStrucMember.idc")
        SetType = Templates.loadTemplateFile("SetType.idc")
        SetStrucComment = Templates.loadTemplateFile("SetStrucComment.idc")
        SetMemberComment = Templates.loadTemplateFile("SetMemberComment.idc")
        data = []
        for val in extractors.addrList:
            cmd = val[0]
            name = val[1]
            addr = val[2]
            if cmd == "MakeName":
                data.append(MakeName.format(addr, name))
            elif cmd == "MakeByte":
                data.append(MakeByte.format(addr))
            elif cmd == "MakeArray":
                data.append(MakeArray.format(addr, val[3]))
            elif cmd == "MakeUnknown":
                data.append(MakeUnknown.format(addr, val[3], val[4]))
            elif cmd == "ExtLine":
                data.append(ExtLine.format(addr, val[3], name))
            elif cmd == "SetFunctionCmt":
                data.append(SetFunctionCmt.format(addr, name, val[3]))
            elif cmd == "MakeDword":
                data.append(MakeDword.format(addr))
            elif cmd == "AddStrucEx":
                data.append(AddStrucEx.format(addr, name))
            elif cmd == "GetStrucIdByName":
                data.append(GetStrucIdByName.format(name))
            elif cmd == "AddStrucMember":
                data.append(AddStrucMember.format(val[3],
                                                  name,
                                                  val[4],
                                                  val[5],
                                                  val[6],
                                                  val[7]))
            elif cmd == "SetType":
                data.append(SetType.format(val[3], val[4], val[5]))
            elif cmd == "SetStrucComment":
                data.append(SetStrucComment.format(val[3], val[4], val[5]))
            elif cmd == "SetMemberComment":
                data.append(SetMemberComment.format(val[3],
                                                    val[4],
                                                    val[5],
                                                    val[6]))
            else:
                print("Error: unknown ida cmd: {0}".format(val))
                exit(1)
        template.data = "".join(data)
        template.save("output/ida", "ida_{0}_{1}.idc", extractors.exe.fileName)
