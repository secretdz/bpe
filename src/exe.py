#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2011-2013 Yommys
# Copyright (C) 2016-2018 Andrei Karas (4144)

import datetime
import os
import struct

from src.section import Section, SectionRange

try:
    xrange
except NameError:
    xrange = range

unpackI = struct.Struct('I').unpack_from
unpackI2 = struct.Struct('II').unpack_from
unpackI4 = struct.Struct('IIII').unpack_from
unpacki4 = struct.Struct('iiii').unpack_from
unpacki = struct.Struct('i').unpack_from
unpackH = struct.Struct('H').unpack_from
unpackB = struct.Struct('B').unpack_from
unpackb = struct.Struct('b').unpack_from


class Exe:
    def __init__(self):
        self.exe = None
        self.size = 0
        self.PEHeader = False
        self.image_base = 0
        self.client_date = 0
        self.sections = dict()
        self.themida = False
        self.fileName = ""
        self.logFile = None
        self.outDir = ""
        self.asprotect = False
        self.fastSearch = False
        self.module = None
        self.initMatch()


    def initMatch(self):
        try:
            from src.module import fastsearch
            if fastsearch.version() >= "00000004":
                self.fastSearch = True
                self.module = fastsearch
                self.readUInt = fastsearch.readUInt
                self.matchWildcard = fastsearch.matchWildcard
                self.match = fastsearch.match
                print("Fast search")
            else:
                print("Slow search")
        except ImportError:
            print("Slow search")


    def log(self, text):
        self.logFile.write(text + "\n")
        print(text)


    def loadSections(self):
        sectionCount = self.read(self.PEHeader + 0x6, 2, "S")
        curSection = self.PEHeader + 0x18 + 0x60 + 0x10 * 0x8
        for idx in range(0, sectionCount):
            section = Section()
            section.name = self.read(curSection, 8).strip()
            section.image_base = self.image_base
            while section.name[-1:] == '\x00':
                section.name = section.name[:-1]
            if section.name == "" or section.name is None:
                section.name = "sect_" + str(idx)
                self.themida = True
            if section.name == ".adata":
                self.log("Warning: found .adata section. " +
                         "Binary probably encoded.")
                self.asprotect = True
            section.vSize = self.readUInt(curSection + 8 + 0 * 4)
            section.vOffset = self.readUInt(curSection + 8 + 1 * 4)
            section.vEnd = section.vOffset + section.vSize
            section.rSize = self.readUInt(curSection + 8 + 2 * 4)
            section.rOffset = self.readUInt(curSection + 8 + 3 * 4)
            section.rEnd = section.rOffset + section.rSize
            section.vrDiff = section.vOffset - section.rOffset
            section.align = 0
            sz = len(self.exe)
            if section.rSize > sz or section.rOffset + section.rSize > sz:
                self.log("Warning: wrong section size {0}".
                         format(section.name))
                section.rSize = sz - section.rOffset
            section.addrRange = SectionRange(section)
            self.sections[section.name] = section
            curSection = curSection + 0x28


    def loadClientDate(self):
        d = datetime.datetime.fromtimestamp(
            self.readUInt(self.PEHeader + 8))
        self.client_date = int(d.strftime("%Y")) * 10000 + \
            int(d.strftime("%m")) * 100 + int(d.strftime("%d"))


    def initExe(self):
        if self.fastSearch:
            self.module.setExe(self.exe)


    def load(self, fileName, outFileName):
        outDir = "output/" + fileName
        with open("clients/" + fileName, "rb") as f:
            self.exe = f.read()
        self.initExe()
        self.size = len(self.exe)
        self.PEHeader = self.match("\x50\x45\x00\x00", 0, self.size - 3)
        if self.PEHeader is False:
            self.log("Error: Cant find PE header. Exiting.")
            exit(1)
        if fileName[-4:] == ".exe":
            fileName = fileName[:-4]
        self.fileName = fileName
        outDir = "output/" + fileName
        if not os.path.exists(outDir):
            os.makedirs(outDir)
        self.logFile = open(outDir + "/" + outFileName, "wt")
        self.outDir = outDir
        self.image_base = self.readUInt(self.PEHeader + 0x34)
        self.loadClientDate()
        if self.client_date == 20041231:
            self.log("Broken client version detected. Fixing to correct one")
            self.client_date = 20120503
        self.log("client name: {0}".format(self.fileName))
        self.log("client date: {0}".format(self.client_date))
        self.loadSections()
        return True


    def test(self, fileName, outFileName):
        outDir = "output/" + fileName
        with open("clients/" + fileName, "rb") as f:
            self.exe = f.read()
        self.initExe()
        self.size = len(self.exe)
        self.PEHeader = self.match("\x50\x45\x00\x00", 0, self.size - 3)
        if self.PEHeader is False:
            self.log("Error: Cant find PE header. Exiting.")
            exit(1)
        if fileName[-4:] == ".exe":
            fileName = fileName[:-4]
        self.fileName = fileName
        outDir = "output/" + fileName
        if not os.path.exists(outDir):
            os.makedirs(outDir)
        self.logFile = open(outDir + "/" + outFileName, "wt")
        self.outDir = outDir
        self.image_base = self.readUInt(self.PEHeader + 0x34)
        self.loadClientDate()
        self.log("exe name: {0}".format(self.fileName))
        section = Section()
        section.name = ".text"
        section.image_base = 0
        section.vSize = self.size
        section.vOffset = 0
        section.vEnd = self.size
        section.rSize = self.size
        section.rOffset = 0
        section.rEnd = self.size
        section.vrDiff = 0
        section.align = 0
        self.sections[section.name] = section
        return True


    def close(self):
        self.logFile.close()
        self.logFile = None
        self.sections = None
        self.exe = None
        self.image_base = None


    def match(self, pattern, start, finish):
        if finish == -1:
            finish = self.size - len(pattern) + 1
        else:
            if finish > self.size - len(pattern) + 1:
                finish = self.size - len(pattern) + 1
        if start >= finish:
            return False
        # print "start - finish: {0} - {1}".format(start, finish)
        # print "search '{0}', from {1} to {2}".\
        #     format(pattern, start, finish + len(pattern) - 1)
        ret = self.exe.find(pattern, start, finish + len(pattern) - 1)
        if ret == -1:
            return False
        return ret


    def matchGen1(self, pattern, start, finish):
        for fpos in xrange(start, finish):
            yield fpos


    def matchGen2(self, pattern, start, finish):
        char = pattern[0]
        exe = self.exe
        idx = exe.find(char, start, finish)
        while idx >= 0:
            # print "found fpos0:{0} = {1}".format(idx, char)
            yield idx
            start = idx + 1
            idx = exe.find(char, start, finish)


    def matchWildcard(self, pattern, wildcard, start, finish):
        exe = self.exe
        diff = self.size - len(pattern) + 1
        if finish > diff:
            finish = diff
        if start >= finish:
            return False
        gen = xrange(1, len(pattern))
        for fpos in self.matchGen2(pattern, start, finish):
            for ppos in gen:
                if exe[fpos + ppos] != pattern[ppos] and \
                   pattern[ppos] != wildcard:
                    break
            else:
                if fpos < 0:
                    return False
                return fpos
        return False


    def matches(self, pattern, start=0, finish=-1):
        offsets = []
        offset = self.match(pattern, start, finish)
        while offset is not False:
            offsets.append(offset)
            offset = self.match(pattern,
                                offset + len(pattern),
                                finish)
        if len(offsets) > 0:
            return offsets
        return False


    def matchesWildcard(self, pattern, wildcard, start=0, finish=-1):
        offsets = []
        offset = self.matchWildcard(pattern, wildcard, start, finish)
        while offset is not False:
            offsets.append(offset)
            offset = self.matchWildcard(pattern,
                                        wildcard,
                                        offset + len(pattern),
                                        finish)
        if len(offsets) > 0:
            return offsets
        return False


    def read(self, offset, size, format=None):
        if offset >= self.size or self.size < 1:
            return False
        if format == "V" or format == "I":
            return unpackI(self.exe, offset)[0]
        elif format == "i":
            return unpacki(self.exe, offset)[0]
        elif format == "S" or format == "H":
            return unpackH(self.exe, offset)[0]
        elif format == "B":
            return unpackB(self.exe, offset)[0]
        elif format == "b":
            return unpackb(self.exe, offset)[0]
        elif format is not None:
            self.log("Error: wrong read format: {0}".format(format))
            exit(1)
        return self.exe[offset:offset + size]


    def readUInt(self, offset):
        if offset >= self.size or self.size < 4:
            return False
        return unpackI(self.exe, offset)[0]


    def readInt(self, offset):
        if offset >= self.size or self.size < 4:
            return False
        return unpacki(self.exe, offset)[0]


    def readUInt2(self, offset):
        if offset >= self.size or self.size < 8:
            return False
        return unpackI2(self.exe, offset)


    def readUInt4(self, offset):
        if offset >= self.size or self.size < 16:
            return False
        return unpackI4(self.exe, offset)


    def readInt4(self, offset):
        if offset >= self.size or self.size < 16:
            return False
        return unpacki4(self.exe, offset)


    def readByte(self, offset):
        if offset >= self.size or self.size < 1:
            return False
        return unpackb(self.exe, offset)[0]


    def readUByte(self, offset):
        if offset >= self.size or self.size < 1:
            return False
        return unpackB(self.exe, offset)[0]


    def readStr(self, offset, strLen=1000):
        idx = self.exe.find('\x00', offset, offset + 1 + strLen)
        if idx is False or idx < 0:
            return False
        if idx >= offset and idx - offset < strLen:
            return self.exe[offset:idx]
        return None


    def detectSection(self, code, cnt=1):
        for key in self.sections:
            section = self.sections[key]
            print(section.name)
            offsets = self.matches(code,
                                   section.rOffset,
                                   section.rOffset + section.rSize)
            if offsets is False:
                print("code found 0 matches")
                continue
            if cnt != -1 and len(offsets) != cnt:
                print("code found {0} matches".format(len(offsets)))
                continue
            return section.name
        return False


    def getCodeSection(self):
        if self.themida is True:
            section = self.sections["sect_0"]
        else:
            section = self.sections[".text"]
        return section


    def codes(self, code, cnt=1, start=-1, finish=-1):
        section = self.getCodeSection()
        if start == -1:
            start = section.rOffset
        if finish == -1:
            finish = section.rOffset + section.rSize
        offsets = self.matches(code,
                               start,
                               finish)
        if offsets is False:
            print("code found 0 matches")
            return False
        if cnt != -1 and len(offsets) != cnt:
            print("code found {0} matches".format(len(offsets)))
            return False
        if cnt == 1:
            return offsets[0]
        else:
            return offsets


    def codesWildcard(self, code, wildcard="", cnt=1, start=-1, finish=-1):
        section = self.getCodeSection()
        if start == -1:
            start = section.rOffset
        if finish == -1:
            finish = section.rOffset + section.rSize
        offsets = self.matchesWildcard(code,
                                       wildcard,
                                       start,
                                       finish)
        if offsets is False:
            print("code found 0 matches")
            return False
        if cnt != -1 and len(offsets) != cnt:
            print("code found {0} matches".format(len(offsets)))
            return False
        if cnt == 1:
            return offsets[0]
        else:
            return offsets


    def code(self, code, start=-1, finish=-1):
        section = self.getCodeSection()
        if start == -1:
            start = section.rOffset
        if finish == -1:
            finish = section.rOffset + section.rSize
        offset = self.match(code, start, finish)
        if offset is False:
            return False
        return offset


    def codeWildcard(self, code, wildcard, start=-1, finish=-1):
        section = self.getCodeSection()
        if start == -1:
            start = section.rOffset
        if finish == -1:
            finish = section.rOffset + section.rSize
        offset = self.matchWildcard(code, wildcard, start, finish)
        if offset is False:
            return False
        return offset


    def stringBySection(self, code, name):
        if name in self.sections:
            section = self.sections[name]
            offset = self.match("\x00" + code + "\x00",
                                section.rOffset,
                                section.rEnd)
            if offset is not False:
                return offset + 1, section
        return False, None


    def string(self, code):
        codeSection = self.getCodeSection()
        offset = self.match("\x00" + code + "\x00",
                            codeSection.rOffset,
                            codeSection.rEnd)
        if offset is not False:
            return offset + 1, codeSection
        offset, section = self.stringBySection(code, ".rdata")
        if offset is not False:
            return offset, section
        offset, section = self.stringBySection(code, ".data")
        if offset is not False:
            return offset, section
        for name in self.sections:
            if name == ".rdata" or name == ".data" or name == codeSection.name:
                continue
            offset, section2 = self.stringBySection(code, name)
            if offset is not False:
                return offset, section2
        return False, None


    def printRawAddr(self, text, offset):
        section = self.getCodeSection()
        self.log(section.printRawAddr(text, offset))


    def rawToVaHex(self, offset):
        section = self.getCodeSection()
        return hex(section.rawToVa(offset))


    def rawToVa(self, offset):
        section = self.getCodeSection()
        return section.rawToVa(offset)


    def vaToRawUnknown(self, offset):
        for key in self.sections:
            section = self.sections[key]
            if section.addrRange.isIn(offset):
                return section.vaToRaw(offset)
        return False


    def getAddrSec(self, offset):
        section = self.getCodeSection()
        return section.getAddrSec(offset)


    def toHex(self, addr, sz):
        addr = struct.pack("I", addr)
        return ("00" * (sz - len(addr))) + addr


    def isClientTooOld(self):
        return self.client_date <= 20100824


    def isClientSkipFlag(self):
        return self.client_date >= 20100817
