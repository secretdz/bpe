#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from src.templates import Templates

import os


class PacketLogger:
    def addLine(self, w, text):
        w.write(text + "\n")


    def bytesHex(self, bytes):
        data = ""
        for byte in bytes:
            data = data + "\\x" + byte.encode("hex")
        return data


    def getScript(self, extractors):
        if len(extractors) == 0:
            return
        template = Templates("file.h", "block.h")
        data = ""
        if os.path.basename(os.path.abspath(".")) == "zero":
            suffix = "_zero"
            fileName = "output/config_buildin_zero.h"
        else:
            suffix = ""
            fileName = "output/config_buildin.h"
        for extractor in extractors:
            if extractor.gCheatDefenderMgr != 0:
                data = data + template.lineTemplate.format(
                    client=extractor.cleanFileName + suffix,
                    flag=1,
                    addr1=hex(extractor.connectionStartupVa),
                    bytes1=self.bytesHex(extractor.packetParser1Bytes),
                    addr2=hex(extractor.hookRecvPacketStartMap),
                    bytes2=self.bytesHex(extractor.packetParser2Bytes),
                    addr3=hex(extractor.hookRecvPacketStartLogin1),
                    bytes3=self.bytesHex(extractor.packetParser3Bytes),
                    addr4=hex(extractor.hookSendEncryptionStart),
                    bytes4=self.bytesHex(extractor.packetParser4Bytes),
                    recvAddr=hex(extractor.recvAddr),
                    sendAddr=hex(extractor.sendAddr),
                    mapExit=hex(extractor.hookRecvPacketExitMap),
                    login1Exit=hex(extractor.hookRecvPacketExitLogin1),
                    login2Start=hex(extractor.hookRecvPacketStartLogin2),
                    login2Exit=hex(extractor.hookRecvPacketExitLogin2),
                    recvPacket=hex(extractor.recvPacketVa),
                    sendPacket=hex(extractor.sendPacketVa),
                    sendExit=hex(extractor.hookSendEncryptionExit),
                )
            else:
                data = data + template.lineTemplate.format(
                    client=extractor.cleanFileName + suffix,
                    flag=0,
                    addr1=hex(extractor.connectionStartupVa),
                    bytes1=self.bytesHex(extractor.packetParser1Bytes),
                    addr2=hex(0),
                    bytes2="",
                    addr3=hex(0),
                    bytes3="",
                    addr4=hex(0),
                    bytes4="",
                    recvAddr=hex(extractor.recvAddr),
                    sendAddr=hex(extractor.sendAddr),
                    mapExit=hex(0),
                    login1Exit=hex(0),
                    login2Start=hex(0),
                    login2Exit=hex(0),
                    recvPacket=hex(0),
                    sendPacket=hex(0),
                    sendExit=hex(0),
                )
        data = template.fileTemplate.format(
            len(extractors),
            extractors[0].packetParser1BytesCount,
            data)
        with open(fileName, "wt") as w:
            w.write(data)


    @staticmethod
    def savePacketLoggerInfo(extractors):
        extractors.cleanupName(extractors.exe.fileName)
        outDir = "output/packetlogger/" + extractors.cleanFileName
        if not os.path.exists(outDir):
            os.makedirs(outDir)
        extractors.packetParser1Bytes = extractors.exe.exe[
            extractors.connectionStartup:extractors.connectionStartup +
            extractors.packetParser1BytesCount]
        if extractors.gCheatDefenderMgr != 0:
            extractors.packetParser2Bytes = extractors.exe.exe[
                extractors.hookRecvPacketStartMapRaw:
                extractors.hookRecvPacketStartMapRaw +
                extractors.packetParser2BytesCount]
            extractors.packetParser3Bytes = extractors.exe.exe[
                extractors.hookRecvPacketStartLogin1Raw:
                extractors.hookRecvPacketStartLogin1Raw +
                extractors.packetParser3BytesCount]
            extractors.packetParser4Bytes = extractors.exe.exe[
                extractors.hookSendEncryptionStartRaw:
                extractors.hookSendEncryptionStartRaw +
                extractors.packetParser4BytesCount]
        with open(outDir + "/Mp3decPlus.cfg", "wt") as w:
            w.write("v1\n{0}\n{1}\n".format(hex(extractors.recvAddr),
                                            hex(extractors.sendAddr)))
