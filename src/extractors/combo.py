#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchCombo(self):
    if self.offset1 is None:
        if self.sendPacket == 0:
            self.comboAddr = 0
            self.exe.log("Packet keys look like not supported. " +
                         "Ignoring combofunction.")
            exit(1)
            return

    if self.sendPacket != 0:
        # 0 MOV ecx, enc_packet_keys
        # 6 PUSH 0
        # 8 CALL combofunction
        # 13 mov ecx, [ebp+buf]
        # 16 xor [ecx], ax
        # 19 cmp dword ptr [esi+m_socket], 0FFFFFFFFh
        code = (
            "\x8B\x0D\xAB\xAB\xAB\x01" +
            "\x6A\x00" +
            "\xE8\xAB\xAB\xAB\xAB" +
            "\x8B\xAB\xAB" +
            "\x66\x31\xAB" +
            "\x83\xAB\xAB\xFF")
        encPacketKeysOffset = 2
        comboOffset = 9
        m_socketOffset = 21
        offset = self.exe.codeWildcard(code,
                                       "\xAB",
                                       self.sendPacket,
                                       self.sendPacket + 0x200)
        if offset is False:
            # 0 MOV ecx, enc_packet_keys
            # 6 PUSH 0
            # 8 CALL combofunction
            # 13 mov ecx, [ebp+buf]
            # 16 xor [ecx], ax
            # 19 cmp dword ptr [esi+m_socket], 0FFFFFFFFh
            code = (
                "\x8B\x0D\xAB\xAB\xAB\x00" +
                "\x6A\x00" +
                "\xE8\xAB\xAB\xAB\xAB" +
                "\x8B\xAB\xAB" +
                "\x66\x31\xAB" +
                "\x83\xAB\xAB\xFF")
            encPacketKeysOffset = 2
            comboOffset = 9
            m_socketOffset = 21
            offset = self.exe.codeWildcard(code,
                                           "\xAB",
                                           self.sendPacket,
                                           self.sendPacket + 0x200)
        if offset is False:
            # 0 mov ecx, enc_packet_keys
            # 6 push ebx
            # 7 push edi
            # 8 push 0
            # 10 call combofunction
            # 15 mov edi, [ebp+Src]
            # 18 mov ebx, [ebp+Size]
            # 21 xor [edi], ax
            code = (
                "\x8B\x0D\xAB\xAB\xAB\x00" +
                "\xAB" +
                "\xAB" +
                "\x6A\x00" +
                "\xE8\xAB\xAB\xAB\xAB" +
                "\xAB\xAB\xAB" +
                "\xAB\xAB\xAB" +
                "\x66\x31")
            encPacketKeysOffset = 2
            comboOffset = 11
            m_socketOffset = 0
            offset = self.exe.codeWildcard(code,
                                           "\xAB",
                                           self.sendPacket,
                                           self.sendPacket + 0x200)
        if offset is False:
            # 0 mov ecx, enc_packet_keys
            # 6 push ebx
            # 7 push edi
            # 8 push 0
            # 10 call combofunction
            # 15 mov edi, [ebp+Src]
            # 18 mov ebx, [ebp+Size]
            # 21 xor [edi], ax
            code = (
                "\x8B\x0D\xAB\xAB\xAB\x01" +
                "\xAB" +
                "\xAB" +
                "\x6A\x00" +
                "\xE8\xAB\xAB\xAB\xAB" +
                "\xAB\xAB\xAB" +
                "\xAB\xAB\xAB" +
                "\x66\x31")
            encPacketKeysOffset = 2
            comboOffset = 11
            m_socketOffset = 0
            offset = self.exe.codeWildcard(code,
                                           "\xAB",
                                           self.sendPacket,
                                           self.sendPacket + 0x200)
        if self.packetVersion <= "20130814":
            if offset is False:
                # 2013-01-03
                # 0  MOV ecx, enc_packet_keys
                # 6  CALL combofunction
                # 11 mov ecx, [esp+4+buf]
                # 15 xor [ecx], ax
                # 18 cmp dword ptr [esi+m_socket], 0FFFFFFFFh
                code = (
                    "\x8B\x0D\xAB\xAB\xAB\x00" +
                    "\xE8\xAB\xAB\xAB\xAB" +
                    "\x8B\xAB\xAB\xAB" +
                    "\x66\x31\xAB" +
                    "\x83\xAB\xAB\xFF")
                encPacketKeysOffset = 2
                comboOffset = 7
                m_socketOffset = 20
                offset = self.exe.codeWildcard(code,
                                               "\xAB",
                                               self.sendPacket,
                                               self.sendPacket + 0x200)
                self.comboSimple = True
            if offset is False:
                # 2013-08-14
                # 0  MOV ecx, enc_packet_keys
                # 6  CALL combofunction
                # 11 mov ecx, [ebp+buf]
                # 14 xor [ecx], ax
                # 17 cmp dword ptr [esi+m_socket], 0FFFFFFFFh
                code = (
                    "\x8B\x0D\xAB\xAB\xAB\x00" +
                    "\xE8\xAB\xAB\xAB\xAB" +
                    "\x8B\xAB\xAB" +
                    "\x66\x31\xAB" +
                    "\x83\xAB\xAB\xFF")
                encPacketKeysOffset = 2
                comboOffset = 7
                m_socketOffset = 19
                offset = self.exe.codeWildcard(code,
                                               "\xAB",
                                               self.sendPacket,
                                               self.sendPacket + 0x200)
                self.comboSimple = True
    if offset is False:
        self.exe.log("Error: failed in seach combofunction")
        self.comboAddr = 0
        if self.packetVersion > "20101116":
            exit(1)
        return
    self.comboAddr = self.getAddr(offset, comboOffset, comboOffset + 4)
    self.addRawFunc("comboFunction", self.comboAddr)
    self.encPacketKeys = self.exe.readUInt(offset + encPacketKeysOffset)
    self.addVaVar("enc_packet_keys", self.encPacketKeys)
    if m_socketOffset != 0:
        m_socket = self.exe.readUByte(offset + m_socketOffset)
        self.addStruct("CRagConnection")
        self.addStructMember("m_socket", m_socket, 4, True)

    if self.offset1 is not None:
        if offset is False:
            # 0 MOV ecx, enc_packet_keys
            # 6 PUSH 1
            # 8 CALL combofunction
            code = (
                "\x8B\x0D\xAB\xAB\xAB\x00" +
                "\x6A\x01" +
                "\xE8\xAB\xAB\xAB\xAB")
            comboOffset = 9
            offset = self.exe.codeWildcard(code,
                                           "\xAB",
                                           self.offset1 - 0x100,
                                           self.offset1)
        if offset is False:
            # 0 MOV ecx, enc_packet_keys
            # 6 PUSH 1
            # 8 CALL combofunction
            code = (
                "\x8B\x0D\xAB\xAB\xAB\x01" +
                "\x6A\x01" +
                "\xE8\xAB\xAB\xAB\xAB")
            comboOffset = 9
            offset = self.exe.codeWildcard(code,
                                           "\xAB",
                                           self.offset1 - 0x100,
                                           self.offset1)
        if offset is False:
            self.exe.log("Error: failed in search combofunction validation")
            self.comboAddr = 0
            exit(1)
        comboAddr = self.getAddr(offset, comboOffset, comboOffset + 4)
        if self.comboAddr != comboAddr:
            self.exe.log("Error: found different comboFunction")
            self.comboAddr = 0
            exit(1)
