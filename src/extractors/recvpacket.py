#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchRecvPacket(self, errorExit, hookErrorExit):
    # 1. search for call pattern
    # 2017-11-01
    # 0  push eax
    # 1  push offet packet_buf
    # 6  call CRagConnection_instanceR
    # 11 mov ecx, eax
    # 13 call CRagConnection_RecvPacket
    # 18 mov byte ptr [ebp + buf + 3], al
    # 21 test al, al
    bufOffset = 2
    instanceOffset = 7
    recvOffset = 14
    hook1Offset = 11
    callRecvPacketOffset = 13
    hook2Offset = 18
    code = (
        "\x50" +
        "\x68\xAB\xAB\xAB\x01" +
        "\xE8\xAB\xAB\xAB\xAB" +
        "\x8B\xC8" +
        "\xE8\xAB\xAB\xAB\xAB" +
        "\x88\x45\x8B" +
        "\x84\xC0")
    offset = self.exe.codeWildcard(code,
                                   "\xAB",
                                   self.gamePollAddr,
                                   self.gamePollAddr + 0x400)
    if offset is False:
        # 2017-11-01
        # 0  push eax
        # 1  push offet packet_buf
        # 6  call CRagConnection_instanceR
        # 11 mov ecx, eax
        # 13 call CRagConnection_RecvPacket
        # 18 mov byte ptr [ebp + buf + 3], al
        # 21 test al, al
        bufOffset = 2
        instanceOffset = 7
        recvOffset = 14
        hook1Offset = 11
        callRecvPacketOffset = 13
        hook2Offset = 18
        code = (
            "\x50" +
            "\x68\xAB\xAB\xAB\x00" +
            "\xE8\xAB\xAB\xAB\xAB" +
            "\x8B\xC8" +
            "\xE8\xAB\xAB\xAB\xAB" +
            "\x88\x45\x8B" +
            "\x84\xC0")
        offset = self.exe.codeWildcard(code,
                                       "\xAB",
                                       self.gamePollAddr,
                                       self.gamePollAddr + 0x400)
    if offset is False and self.packetVersion < "20160000":
        # 2015-01-07
        # 0  push ecx
        # 1  push offet packet_buf
        # 6  call CRagConnection_instanceR
        # 11 mov ecx, eax
        # 13 call CRagConnection_RecvPacket
        # 18 mov bl, al
        # 21 test bl, bl
        bufOffset = 2
        instanceOffset = 7
        recvOffset = 14
        hook1Offset = 11
        callRecvPacketOffset = 13
        hook2Offset = 18
        code = (
            "\x51" +
            "\x68\xAB\xAB\xAB\x00" +
            "\xE8\xAB\xAB\xAB\xAB" +
            "\x8B\xC8" +
            "\xE8\xAB\xAB\xAB\xAB" +
            "\x8A\xAB" +
            "\x84\xAB")
        offset = self.exe.codeWildcard(code,
                                       "\xAB",
                                       self.gamePollAddr,
                                       self.gamePollAddr + 0x400)
        if offset is False:
            # 2011-01-04
            # 0  push offet packet_buf
            # 5  call CRagConnection_instanceR
            # 10 mov ecx, eax
            # 12 call CRagConnection_RecvPacket
            # 17 test al, al
            bufOffset = 1
            instanceOffset = 6
            recvOffset = 13
            hook1Offset = 10
            callRecvPacketOffset = 12
            hook2Offset = 17
            code = (
                "\x68\xAB\xAB\xAB\x00" +
                "\xE8\xAB\xAB\xAB\xAB" +
                "\x8B\xC8" +
                "\xE8\xAB\xAB\xAB\xAB" +
                "\x84\xAB")
            offset = self.exe.codeWildcard(code,
                                           "\xAB",
                                           self.gamePollAddr,
                                           self.gamePollAddr + 0x400)
    if offset is False:
        self.exe.log("failed searchRecvPacket in step 1.")
        if errorExit is True:
            exit(1)
        return False
    # 2. compare is first call is really CRagConnection::instanceR
    if self.instanceR != 0:
        if self.getAddr(offset,
                        instanceOffset,
                        instanceOffset + 4
                        ) != self.instanceR:
            self.exe.log("failed searchRecvPacket in step 2.")
            if errorExit is True:
                exit(1)
            return False
    self.recvPacket = self.getAddr(offset, recvOffset, recvOffset + 4)
    self.recvPacketVa = self.exe.rawToVa(
        self.getAddr(offset, recvOffset, recvOffset + 4))
    self.addRawFunc("CRagConnection::RecvPacket",
                    self.recvPacket)
    if hook1Offset == 0 or hook2Offset == 0:
        if self.packetVersion >= "20180315":
            self.exe.log("hooks for CRagConnection_RecvPacket not found")
            if hookErrorExit is True:
                exit(1)
    else:
        self.hookRecvPacketStartMapRaw = offset + hook1Offset
        self.hookRecvPacketStartMap = self.exe.rawToVa(offset) + hook1Offset
        self.hookRecvPacketExitMap = self.exe.rawToVa(offset) + hook2Offset
        self.showVaAddr("hookRecvPacketStartMap",
                        self.hookRecvPacketStartMap)
        self.showVaAddr("hookRecvPacketExitMap",
                        self.hookRecvPacketExitMap)
    packetBuf = self.exe.readUInt(offset + bufOffset)
    if self.packetBuf != 0:
        if self.packetBuf != packetBuf:
            self.exe.log("Error: detected wrong packet_buf.")
            if errorExit is True:
                exit(1)
    else:
        self.packetBuf = packetBuf
    if callRecvPacketOffset != 0:
        self.callRecvPacket1 = offset + callRecvPacketOffset
    return True
