#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchCharInfo(self, errorExit):
    # search g_charInfo in process_recv_packet_6D
    # 2016 +
    # 0  lea esi, [edx+2]
    # 3  mov ecx, 26h
    # 8  mov edi, offset g_charInfo
    # 13 rep movsd
    # 15 movsw
    # 17 movsb
    # 18 mov eax, [ebx+CLoginMode.m_numChars2]
    # 24 imul eax, 9Bh
    # 30 lea edi, [ebx+CLoginMode.m_charInfo]
    # 36 add edi, eax
    # 38 lea esi, [edx+2]
    # 41 mov ecx, 26h
    # 46 rep movsd
    # 48 movsw
    # 50 movsb
    # 51 mov eax, [ebx+CLoginMode.m_numChars2]
    # 57 imul eax, 9Bh
    # 63 lea esi, [ebx+CLoginMode.m_charInfo.map_name]
    charInfoOffset = 9
    CLoginMode_m_numChars2Offset1 = 20
    CHARACTER_INFO_SizeOffset1 = 26
    CLoginMode_m_charInfoOffset = 32
    CLoginMode_m_numChars2Offset2 = 53
    CHARACTER_INFO_SizeOffset2 = 59
    CLoginMode_m_charInfo_map_nameOffset = 65
    sizeCntOffset1 = 0
    sizeCntOffset2 = 0
    packetBuf2Offset1 = 0
    packetBuf2Offset2 = 0
    code = (
        "\x8D\xAB\x02" +
        "\xB9\xAB\x00\x00\x00" +
        "\xBF\xAB\xAB\xAB\xAB" +
        "\xF3\xA5" +
        "\x66\xA5" +
        "\xA4" +
        "\x8B\xAB\xAB\xAB\x00\x00" +
        "\x69\xAB\xAB\xAB\x00\x00" +
        "\x8D\xAB\xAB\xAB\x00\x00" +
        "\x03\xAB" +
        "\x8D\xAB\x02" +
        "\xB9\xAB\x00\x00\x00" +
        "\xF3\xA5" +
        "\x66\xA5" +
        "\xA4" +
        "\x8B\xAB\xAB\xAB\x00\x00" +
        "\x69\xAB\xAB\xAB\x00\x00" +
        "\x8D\xAB\xAB\xAB\x00\x00")
    offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 2015-03-11 -
        # 0  lea esi, [eax+2]
        # 3  mov ecx, 24h
        # 8  mov edi, offset g_charInfo
        # 13 rep movsd
        # 15 movsw
        # 17 movsb
        # 18 mov ecx, [ebx+CLoginMode.m_numChars2]
        # 24 imul ecx, 93h
        # 30 lea edi, [ecx+ebx+CLoginMode.m_charInfo]
        # 37 lea esi, [eax+2]
        # 40 mov ecx, 24h
        # 45 rep movsd
        # 47 movsw
        # 49 movsb
        # 50 mov edx, [ebx+CLoginMode.m_numChars2]
        # 56 imul edx, 93h
        # 62 lea esi, [edx+ebx+CLoginMode.m_charInfo.map_name]
        charInfoOffset = 9
        CLoginMode_m_numChars2Offset1 = 20
        CHARACTER_INFO_SizeOffset1 = 26
        CLoginMode_m_charInfoOffset = 33
        CLoginMode_m_numChars2Offset2 = 52
        CHARACTER_INFO_SizeOffset2 = 58
        CLoginMode_m_charInfo_map_nameOffset = 65
        sizeCntOffset1 = 0
        sizeCntOffset2 = 0
        packetBuf2Offset1 = 0
        packetBuf2Offset2 = 0
        code = (
            "\x8D\xAB\x02" +
            "\xB9\xAB\x00\x00\x00" +
            "\xBF\xAB\xAB\xAB\xAB" +
            "\xF3\xA5" +
            "\x66\xA5" +
            "\xA4" +
            "\x8B\xAB\xAB\xAB\x00\x00" +
            "\x69\xAB\xAB\xAB\x00\x00" +
            "\x8D\xAB\xAB\xAB\xAB\x00\x00" +
            "\x8D\xAB\x02" +
            "\xB9\xAB\x00\x00\x00" +
            "\xF3\xA5" +
            "\x66\xA5" +
            "\xA4" +
            "\x8B\xAB\xAB\xAB\x00\x00" +
            "\x69\xAB\xAB\xAB\x00\x00" +
            "\x8D\xAB\xAB\xAB\xAB\x00\x00")
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 2014-08-14 -
        # 0  lea esi, [eax+2]
        # 3  mov ecx, 24h
        # 8  mov edi, offset g_charInfo
        # 13 rep movsd
        # 15 mov ecx, [ebx+CLoginMode.m_numChars2]
        # 21 lea ecx, [ecx+ecx*8]
        # 24 add ecx, ecx
        # 26 lea edi, [ebx+ecx*8+CLoginInfo->m_charInfo]
        # 33 lea esi, [eax+2]
        # 36 mov ecx, 24h
        # 41 rep movsd
        # 43 mov eax, [ebx+CLoginMode.m_numChars2]
        # 49 lea edx, [eax+eax*8]
        # 52 add edx, edx
        # 54 lea esi, [ebx+edx*8+CLoginInfo->m_charInfo->mapName]
        sizeCntOffset1 = 4
        charInfoOffset = 9
        CLoginMode_m_numChars2Offset1 = 17
        CHARACTER_INFO_SizeOffset1 = 0
        CLoginMode_m_charInfoOffset = 29
        sizeCntOffset2 = 37
        CLoginMode_m_numChars2Offset2 = 45
        CHARACTER_INFO_SizeOffset2 = 0
        CLoginMode_m_charInfo_map_nameOffset = 57
        packetBuf2Offset1 = 0
        packetBuf2Offset2 = 0
        code = (
            "\x8D\xAB\x02" +
            "\xB9\xAB\x00\x00\x00" +
            "\xBF\xAB\xAB\xAB\xAB" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00" +
            "\x8D\xAB\xAB" +
            "\x03\xC9" +
            "\x8D\xAB\xAB\xAB\xAB\x00\x00" +
            "\x8D\xAB\x02" +
            "\xB9\xAB\x00\x00\x00" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00" +
            "\x8D\x14\xC0" +
            "\x03\xD2" +
            "\x8D\xAB\xAB\xAB\xAB\x00\x00")
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 2013-06-05 -
        # 0  mov esi, eax
        # 2  mov ecx, 24h
        # 7  mov edi, offset g_charInfo
        # 12 rep movsd
        # 14 mov ecx, [ebx+CLoginMode.m_numChars2]
        # 20 lea ecx, [ecx+ecx*8]
        # 23 shl ecx, 4
        # 26 lea edi, [ecx+ebx+CLoginInfo->m_charInfo]
        # 33 mov esi, eax
        # 35 mov ecx, 24h
        # 40 rep movsd
        # 42 mov eax, [ebx+CLoginMode.m_numChars2]
        # 48 lea edx, [eax+eax*8]
        # 51 shl edx, 4
        # 54 lea esi, [edx+ebx+CLoginInfo->m_charInfo->mapName]
        sizeCntOffset1 = 3
        charInfoOffset = 8
        CLoginMode_m_numChars2Offset1 = 16
        CHARACTER_INFO_SizeOffset1 = 0
        CLoginMode_m_charInfoOffset = 29
        sizeCntOffset2 = 36
        CLoginMode_m_numChars2Offset2 = 44
        CHARACTER_INFO_SizeOffset2 = 0
        CLoginMode_m_charInfo_map_nameOffset = 57
        packetBuf2Offset1 = 0
        packetBuf2Offset2 = 0
        code = (
            "\x8B\xAB" +
            "\xB9\xAB\x00\x00\x00" +
            "\xBF\xAB\xAB\xAB\xAB" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00" +
            "\x8D\xAB\xAB" +
            "\xC1\xAB\x04" +
            "\x8D\xAB\xAB\xAB\xAB\x00\x00" +
            "\x8B\xAB" +
            "\xB9\xAB\x00\x00\x00" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00" +
            "\x8D\x14\xC0" +
            "\xC1\xAB\x04" +
            "\x8D\xAB\xAB\xAB\xAB\x00\x00")
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 2011-10-25 -
        # 0  mov esi, eax
        # 2  mov ecx, 22h
        # 7  mov edi, offset g_charInfo
        # 12 rep movsd
        # 14 mov ecx, [ebx+CLoginMode.m_numChars2]
        # 20 imul ecx, 88h
        # 26 lea edi, [ecx+ebx+CLoginMode.m_charInfo]
        # 33 mov esi, eax
        # 35 mov ecx, 22h
        # 40 rep movsd
        # 42 mov edx, [ebx+CLoginMode.m_numChars2]
        # 48 imul edx, 88h
        # 54 lea esi, [edx+ebx+CLoginMode.m_charInfo.map_name]
        charInfoOffset = 8
        CLoginMode_m_numChars2Offset1 = 16
        CHARACTER_INFO_SizeOffset1 = 22
        CLoginMode_m_charInfoOffset = 29
        CLoginMode_m_numChars2Offset2 = 44
        CHARACTER_INFO_SizeOffset2 = 50
        CLoginMode_m_charInfo_map_nameOffset = 57
        sizeCntOffset1 = 0
        sizeCntOffset2 = 0
        packetBuf2Offset1 = 0
        packetBuf2Offset2 = 0
        code = (
            "\x8B\xAB" +
            "\xB9\xAB\x00\x00\x00" +
            "\xBF\xAB\xAB\xAB\xAB" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00" +
            "\x69\xAB\xAB\xAB\x00\x00" +
            "\x8D\xAB\xAB\xAB\xAB\x00\x00" +
            "\x8B\xAB" +
            "\xB9\xAB\x00\x00\x00" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00" +
            "\x69\xAB\xAB\xAB\x00\x00" +
            "\x8D\xAB\xAB\xAB\xAB\x00\x00")
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 2010-09-28 -
        # 0  mov ecx, 21h
        # 5  mov esi, offset packet_buf + 2
        # 10 mov edi, offset g_charInfo
        # 15 rep movsd
        # 17 mov eax, [ebx+CLoginMode.m_numChars2]
        # 23 mov esi, offset packet_buf + 2
        # 28 mov ecx, eax
        # 30 shl ecx, 5
        # 33 add ecx, eax
        # 35 lea edi, [ebx+ecx*4+CLoginInfo->m_charInfo]
        # 42 mov ecx, 21h
        # 47 rep movsd
        # 49 mov eax, [ebx+CLoginMode.m_numChars2]
        # 55 or ecx, 0FFFFFFFFh
        # 58 mov edx, eax
        # 60 shl edx, 5
        # 63 add edx, eax
        # 65 xor eax, eax
        # 67 lea esi, [ebx+edx*4+CLoginInfo->m_charInfo->mapName]
        sizeCntOffset1 = 1
        charInfoOffset = 11
        CLoginMode_m_numChars2Offset1 = 19
        CHARACTER_INFO_SizeOffset1 = 0
        CLoginMode_m_charInfoOffset = 38
        sizeCntOffset2 = 43
        CLoginMode_m_numChars2Offset2 = 51
        CHARACTER_INFO_SizeOffset2 = 0
        CLoginMode_m_charInfo_map_nameOffset = 70
        packetBuf2Offset1 = 6
        packetBuf2Offset2 = 24
        code = (
            "\xB9\xAB\x00\x00\x00" +
            "\xBE\xAB\xAB\xAB\xAB" +
            "\xBF\xAB\xAB\xAB\xAB" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00" +
            "\xBE\xAB\xAB\xAB\xAB" +
            "\x8B\xAB" +
            "\xC1\xAB\x05" +
            "\xAB\xAB" +
            "\x8D\xAB\xAB\xAB\xAB\x00\x00" +
            "\xB9\xAB\x00\x00\x00" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00" +
            "\x83\xAB\xFF" +
            "\x8B\xAB" +
            "\xC1\xAB\x05" +
            "\x03\xAB" +
            "\x33\xAB" +
            "\x8D\xAB\xAB\xAB\xAB\x00\x00")
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 2010-08-12 -
        # 0  mov ecx, 21h
        # 5  lea esi, [ebp+packet_buf+2]   (+++ unused for now)
        # 11 mov edi, offset g_charInfo
        # 16 rep movsd
        # 18 mov eax, [ebx+CLoginMode.m_numChars2]
        # 24 mov ecx, 21h
        # 29 mov edx, eax
        # 31 lea esi, [ebp+packet_buf+2]   (+++ unused for now)
        # 37 shl edx, 5
        # 40 add edx, eax
        # 42 lea edi, [ebx+edx*4+CLoginInfo->m_charInfo]
        # 49 rep movsd
        # 51 mov eax, [ebx+CLoginMode.m_numChars2]
        # 57 mov ecx, eax
        # 59 shl ecx, 5
        # 62 add ecx, eax
        # 64 xor eax, eax
        # 66 lea esi, [ebx+ecx*4+CLoginInfo->m_charInfo->mapName]
        sizeCntOffset1 = 1
        charInfoOffset = 12
        CLoginMode_m_numChars2Offset1 = 20
        CHARACTER_INFO_SizeOffset1 = 0
        CLoginMode_m_charInfoOffset = 45
        sizeCntOffset2 = 25
        CLoginMode_m_numChars2Offset2 = 53
        CHARACTER_INFO_SizeOffset2 = 0
        CLoginMode_m_charInfo_map_nameOffset = 69
        packetBuf2Offset1 = 0
        packetBuf2Offset2 = 0
        code = (
            "\xB9\xAB\x00\x00\x00" +
            "\x8D\xAB\xAB\xAB\xAB\xAB" +
            "\xBF\xAB\xAB\xAB\xAB" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00" +
            "\xB9\xAB\x00\x00\x00" +
            "\x8B\xAB" +
            "\x8D\xAB\xAB\xAB\xAB\xAB" +
            "\xC1\xAB\x05" +
            "\x03\xAB" +
            "\x8D\xAB\xAB\xAB\xAB\x00\x00" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00" +
            "\x8B\xAB" +
            "\xC1\xAB\x05" +
            "\x03\xAB" +
            "\x33\xAB" +
            "\x8D\xAB\xAB\xAB\xAB\x00\x00")
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 2010-07-30 -
        # 0  mov ecx, 1Ch
        # 5  lea esi, [ebp+packet_buf+2]   (+++ unused for now)
        # 11 mov edi, offset g_charInfo
        # 16 rep movsd
        # 18 mov eax, [ebx+CLoginMode.m_numChars2]
        # 24 mov ecx, 1Ch
        # 29 lea esi, [ebp+packet_buf+2]   (+++ unused for now)
        # 35 lea edx, ds:0[eax*8]
        # 42 sub edx, eax
        # 44 shl edx, 4
        # 47 lea edi, [ebx+edx*4+CLoginInfo->m_charInfo]
        # 54 rep movsd
        # 56 mov eax, [ebx+CLoginMode.m_numChars2]
        # 62 inc ecx
        sizeCntOffset1 = 1
        charInfoOffset = 12
        CLoginMode_m_numChars2Offset1 = 20
        CHARACTER_INFO_SizeOffset1 = 0
        CLoginMode_m_charInfoOffset = 50
        sizeCntOffset2 = 25
        CLoginMode_m_numChars2Offset2 = 58
        CHARACTER_INFO_SizeOffset2 = 0
        CLoginMode_m_charInfo_map_nameOffset = 0
        packetBuf2Offset1 = 0
        packetBuf2Offset2 = 0
        code = (
            "\xB9\xAB\x00\x00\x00" +
            "\x8D\xAB\xAB\xAB\xAB\xAB" +
            "\xBF\xAB\xAB\xAB\xAB" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00" +
            "\xB9\xAB\x00\x00\x00" +
            "\x8D\xAB\xAB\xAB\xAB\xAB" +
            "\x8D\xAB\xAB\xAB\xAB\x00\x00" +
            "\x2B\xAB" +
            "\xC1\xAB\x04" +
            "\x8D\xAB\xAB\xAB\xAB\x00\x00" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00")
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 2010-07-27 -
        # 0  mov ecx, 20h
        # 5  lea esi, [ebp+packet_buf+2]   (+++ unused for now)
        # 11 mov edi, offset g_charInfo
        # 16 rep movsd
        # 18 mov eax, [ebx+CLoginMode.m_numChars2]
        # 24 mov ecx, 20h
        # 29 shl edx, 7
        # 32 lea esi, [ebp+packet_buf+2]   (+++ unused for now)
        # 38 lea edi, [ebx+edx*4+CLoginInfo->m_charInfo]
        # 45 rep movsd
        # 47 mov eax, [ebx+CLoginMode.m_numChars2]
        # 53 inc ecx
        sizeCntOffset1 = 1
        charInfoOffset = 12
        CLoginMode_m_numChars2Offset1 = 20
        CHARACTER_INFO_SizeOffset1 = 0
        CLoginMode_m_charInfoOffset = 41
        sizeCntOffset2 = 25
        CLoginMode_m_numChars2Offset2 = 49
        CHARACTER_INFO_SizeOffset2 = 0
        CLoginMode_m_charInfo_map_nameOffset = 0
        packetBuf2Offset1 = 0
        packetBuf2Offset2 = 0
        code = (
            "\xB9\xAB\x00\x00\x00" +
            "\x8D\xAB\xAB\xAB\xAB\xAB" +
            "\xBF\xAB\xAB\xAB\xAB" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00" +
            "\xB9\xAB\x00\x00\x00" +
            "\xC1\xAB\x07" +
            "\x8D\xAB\xAB\xAB\xAB\xAB" +
            "\x8D\xAB\xAB\xAB\xAB\x00\x00" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00")
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 2010-06-08 -
        # 0  mov ecx, 1Ch
        # 5  lea esi, [ebp+packet_buf+2]   (+++ unused for now)
        # 11 mov edi, offset g_charInfo
        # 16 rep movsd
        # 18 mov eax, [ebx+CLoginMode.m_numChars2]
        # 24 lea esi, [ebp+packet_buf+2]   (+++ unused for now)
        # 30 lea ecx, ds:0[eax*8]
        # 37 sub ecx, eax
        # 39 shl ecx, 4
        # 42 lea edi, [ebx+edx*4+CLoginInfo->m_charInfo]
        # 49 mov ecx, 1Ch
        # 54 rep movsd
        # 56 mov eax, [ecx+CLoginMode.m_numChars2]
        # 62 inc ecx
        sizeCntOffset1 = 1
        charInfoOffset = 12
        CLoginMode_m_numChars2Offset1 = 20
        CHARACTER_INFO_SizeOffset1 = 0
        CLoginMode_m_charInfoOffset = 45
        sizeCntOffset2 = 50
        CLoginMode_m_numChars2Offset2 = 58
        CHARACTER_INFO_SizeOffset2 = 0
        CLoginMode_m_charInfo_map_nameOffset = 0
        packetBuf2Offset1 = 0
        packetBuf2Offset2 = 0
        code = (
            "\xB9\xAB\x00\x00\x00" +
            "\x8D\xAB\xAB\xAB\xAB\xAB" +
            "\xBF\xAB\xAB\xAB\xAB" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00" +
            "\x8D\xAB\xAB\xAB\xAB\xAB" +
            "\x8D\xAB\xAB\xAB\xAB\x00\x00" +
            "\xAB\xAB" +
            "\xC1\xAB\x04" +
            "\x8D\xAB\xAB\xAB\xAB\x00\x00" +
            "\xB9\xAB\x00\x00\x00" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00")
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 2009-07-08 -
        # 0  mov ecx, 1Ch
        # 5  mov esi, edx
        # 7  mov edi, offset g_charInfo
        # 12 rep movsd
        # 14 mov eax, [ebx+CLoginMode.m_numChars2]
        # 20 imul ecx, 70h
        # 23 lea edi, [ebx+edx*4+CLoginInfo->m_charInfo]
        # 30 mov ecx, 1Ch
        # 35 mov esi, edx
        # 37 rep movsd
        # 39 mov ecx, 1
        # 44 add [eax+CLoginMode.m_numChars2], ecx
        sizeCntOffset1 = 1
        charInfoOffset = 8
        CLoginMode_m_numChars2Offset1 = 16
        CHARACTER_INFO_SizeOffset1 = 22
        CLoginMode_m_charInfoOffset = 26
        sizeCntOffset2 = 31
        CLoginMode_m_numChars2Offset2 = 46
        CHARACTER_INFO_SizeOffset2 = 0
        CLoginMode_m_charInfo_map_nameOffset = 0
        packetBuf2Offset1 = 0
        packetBuf2Offset2 = 0
        code = (
            "\xB9\xAB\x00\x00\x00" +
            "\x8B\xAB" +
            "\xBF\xAB\xAB\xAB\xAB" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00" +
            "\x6B\xAB\xAB"
            "\x8D\xAB\xAB\xAB\xAB\x00\x00" +
            "\xB9\xAB\x00\x00\x00" +
            "\x8B\xAB" +
            "\xF3\xA5" +
            "\xB9\x01\x00\x00\x00" +
            "\x01\xAB\xAB\xAB\x00\x00")
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 2009-06-10 -
        # 0  mov ecx, 1Bh
        # 5  lea esi, [ebp+packet_buf+2]   (+++ unused for now)
        # 11 mov edi, offset g_charInfo
        # 16 rep movsd
        # 18 mov eax, [ebx+CLoginMode.m_numChars2]
        # 24 lea esi, [ebp+packet_buf+2]   (+++ unused for now)
        # 30 lea eax, [eax+eax*2]
        # 33 lea ecx, [eax+eax*8]
        # 36 lea edi, [ebx+edx*4+CLoginInfo->m_charInfo]
        # 43 mov ecx, 1Bh
        # 48 rep movsd
        # 50 mov eax, [ecx+CLoginMode.m_numChars2]
        # 56 inc ecx
        sizeCntOffset1 = 1
        charInfoOffset = 12
        CLoginMode_m_numChars2Offset1 = 20
        CHARACTER_INFO_SizeOffset1 = 0
        CLoginMode_m_charInfoOffset = 39
        sizeCntOffset2 = 44
        CLoginMode_m_numChars2Offset2 = 52
        CHARACTER_INFO_SizeOffset2 = 0
        CLoginMode_m_charInfo_map_nameOffset = 0
        packetBuf2Offset1 = 0
        packetBuf2Offset2 = 0
        code = (
            "\xB9\xAB\x00\x00\x00" +
            "\x8D\xAB\xAB\xAB\xAB\xAB" +
            "\xBF\xAB\xAB\xAB\xAB" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00" +
            "\x8D\xAB\xAB\xAB\xAB\xAB" +
            "\x8D\xAB\xAB" +
            "\x8D\xAB\xAB" +
            "\x8D\xAB\xAB\xAB\xAB\x00\x00" +
            "\xB9\xAB\x00\x00\x00" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00")
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 2009-01-13 -
        # 0  mov ecx, 1Ch
        # 5  mov esi, edx
        # 7  mov edi, offset g_charInfo
        # 12 rep movsd
        # 14 mov eax, [ebx+CLoginMode.m_numChars2]
        # 20 imul ecx, 70h
        # 23 lea edi, [ebx+edx*4+CLoginInfo->m_charInfo]
        # 30 mov ecx, 1Ch
        # 35 mov esi, edx
        # 37 rep movsd
        # 39 inc [eax+CLoginMode.m_numChars2], ecx
        sizeCntOffset1 = 1
        charInfoOffset = 8
        CLoginMode_m_numChars2Offset1 = 16
        CHARACTER_INFO_SizeOffset1 = 22
        CLoginMode_m_charInfoOffset = 26
        sizeCntOffset2 = 31
        CLoginMode_m_numChars2Offset2 = 41
        CHARACTER_INFO_SizeOffset2 = 0
        CLoginMode_m_charInfo_map_nameOffset = 0
        packetBuf2Offset1 = 0
        packetBuf2Offset2 = 0
        code = (
            "\xB9\xAB\x00\x00\x00" +
            "\x8B\xAB" +
            "\xBF\xAB\xAB\xAB\xAB" +
            "\xF3\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00" +
            "\x6B\xAB\xAB"
            "\x8D\xAB\xAB\xAB\xAB\x00\x00" +
            "\xB9\xAB\x00\x00\x00" +
            "\x8B\xAB" +
            "\xF3\xA5" +
            "\xFF\xAB\xAB\xAB\x00\x00")
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 2006-10-23 -
        # 0  mov ecx, 1Ah
        # 5  lea esi, [ebp+packet_buf+2]   (+++ unused for now)
        # 11 mov edi, offset g_charInfo
        # 16 rep movsd
        # 18 movsw
        # 20 mov eax, [ebx+CLoginMode.m_numChars2]
        # 26 lea esi, [ebp+packet_buf+2]   (+++ unused for now)
        # 32 lea ecx, [eax+eax*2]
        # 35 lea ecx, [ecx+ecx*8]
        # 38 shl ecx, 1
        # 40 sub ecx, eax
        # 42 lea edi, [ebx+ecx*2+CLoginInfo->m_charInfo]
        # 49 mov ecx, 1Ah
        # 54 rep movsd
        # 56 movsw
        # 58 mov eax, [ebx+CLoginMode.m_numChars2]
        sizeCntOffset1 = 1
        charInfoOffset = 12
        CLoginMode_m_numChars2Offset1 = 22
        CHARACTER_INFO_SizeOffset1 = 0
        CLoginMode_m_charInfoOffset = 45
        sizeCntOffset2 = 50
        CLoginMode_m_numChars2Offset2 = 60
        CHARACTER_INFO_SizeOffset2 = 0
        CLoginMode_m_charInfo_map_nameOffset = 0
        packetBuf2Offset1 = 0
        packetBuf2Offset2 = 0
        code = (
            "\xB9\xAB\x00\x00\x00" +
            "\x8D\xAB\xAB\xAB\xAB\xAB" +
            "\xBF\xAB\xAB\xAB\xAB" +
            "\xF3\xA5" +
            "\x66\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00" +
            "\x8D\xB5\xAB\xAB\xAB\xAB" +
            "\xAB\xAB\xAB" +
            "\xAB\xAB\xAB" +
            "\xD1\xAB" +
            "\x2B\xAB" +
            "\x8D\xAB\xAB\xAB\xAB\x00\x00" +
            "\xB9\xAB\x00\x00\x00" +
            "\xF3\xA5" +
            "\x66\xA5" +
            "\x8B\xAB\xAB\xAB\x00\x00")
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        self.exe.log("Error: failed in seach g_CharInfo")
        if errorExit is True:
            exit(1)
        return
    self.gcharInfo = self.exe.readUInt(offset + charInfoOffset)
    self.addVaVar("g_charInfo", self.gcharInfo)
    numChars1 = self.exe.readUInt(offset + CLoginMode_m_numChars2Offset1)
    numChars2 = self.exe.readUInt(offset + CLoginMode_m_numChars2Offset2)
    if numChars1 != numChars2:
        self.exe.log("Error: found different CLoginMode.m_numChars2 offsets")
        exit(1)
    self.CLoginMode_m_numChars2 = numChars1
    self.addStruct("CLoginMode")
    self.addStructMember("m_numChars2", numChars1, 4)
    if CHARACTER_INFO_SizeOffset1 != 0 and CHARACTER_INFO_SizeOffset2 != 0:
        CHAR_Size1 = self.exe.readUInt(offset + CHARACTER_INFO_SizeOffset1)
        CHAR_Size2 = self.exe.readUInt(offset + CHARACTER_INFO_SizeOffset2)
    else:
        CHAR_Size1 = self.exe.readUInt(offset + sizeCntOffset1)
        CHAR_Size2 = self.exe.readUInt(offset + sizeCntOffset2)
        CHAR_Size1 = CHAR_Size1 * 4
        CHAR_Size2 = CHAR_Size2 * 4
    if CHAR_Size1 != CHAR_Size2:
        self.exe.log("Error: found different CHARACTER_INFO sizes")
        exit(1)
    if CHARACTER_INFO_SizeOffset1 != 0 and CHARACTER_INFO_SizeOffset2 == 0:
        CHAR_Size3 = self.exe.read(offset + CHARACTER_INFO_SizeOffset1,
                                   1,
                                   'B')
        if CHAR_Size1 != CHAR_Size3:
            self.exe.log("Error: found different CHARACTER_INFO sizes")
            exit(1)
    self.CHARACTER_INFO_Size = CHAR_Size1
    if packetBuf2Offset1 != 0:
        packetBuf1 = self.exe.readUInt(offset + packetBuf2Offset1) - 2
        packetBuf2 = self.exe.readUInt(offset + packetBuf2Offset2) - 2
        if packetBuf1 != packetBuf2:
            self.exe.log("Error: found different packet_buf's.")
            exit(1)
        if self.packetBuf != 0:
            if self.packetBuf != packetBuf1:
                self.exe.log("Error: found wrong packet_buf.")
                exit(1)
        else:
            self.packetBuf = packetBuf1
            self.addVaVar("packet_buf", self.packetBuf)
    self.addStruct("CHARACTER_INFO")
    self.setStructComment("Size {0}".format(hex(self.CHARACTER_INFO_Size)))
    self.addStructMember("padding_end", self.CHARACTER_INFO_Size - 1, 1)
    self.showVaAddr("len(CHARACTER_INFO)", self.CHARACTER_INFO_Size)
    self.CLoginMode_m_charInfo = \
        self.exe.readUInt(offset + CLoginMode_m_charInfoOffset)
    self.addStruct("CLoginMode")
    self.addStructMember("m_charInfo", self.CLoginMode_m_charInfo, 4)
    if CLoginMode_m_charInfo_map_nameOffset != 0:
        self.CLoginMode_m_charInfo_map_name = \
            self.exe.readUInt(offset + CLoginMode_m_charInfo_map_nameOffset)
        self.addStruct("CHARACTER_INFO")
        diff = self.CLoginMode_m_charInfo_map_name - self.CLoginMode_m_charInfo
        self.addStructMember("map_name",
                             diff,
                             16,
                             True)
