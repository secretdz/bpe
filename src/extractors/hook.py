#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchHookAddr(self):
    # if self.comboAddr == 0:
    #       self.exe.log("Skip searchHookAddr, because no combofunction")
    #       self.hookAddr = 0
    #       self.hookAddr2 = 0
    #       return
    addrSet = set()
    addrList = dict()
    for addr in self.allCallAddrs:
        if addr not in addrSet:
            addrSet.add(addr)
            addrList[addr] = 1
        else:
            addrList[addr] = addrList[addr] + 1
    addr = self.allCallAddrs[2]
    if addrList[addr] < 3:
        self.exe.log("Cant find hook2 function")
        exit(1)
    self.hookAddr2 = addr
    # push eax
    # lea ecx, [ebp + addr1]
    # call hookaddr
    # mov [ebp + addr2], 0FFFFFFFFh
    code = (
        "\x50" +
        "\x8D\x4D\xAB" +
        "\xE8\xAB\xAB\x00\x00" +
        "\xC7\x45\xAB\xFF\xFF\xFF\xFF")
    addrOffset = 5
    self.hookAddrOffset = 8
    offset = self.exe.codeWildcard(code,
                                   "\xAB",
                                   self.hookAddr2,
                                   self.hookAddr2 + 100)
    if offset is False:
        # 2014-05-08
        # push eax
        # lea ecx, [esi + A]
        # push ecx
        # add edi, B
        # push edi
        # call hookaddr
        # add esp, C
        # mov [ebp + addr2], 0FFFFFFFFh
        code = (
            "\x50" +
            "\x8D\x4E\xAB" +
            "\x51" +
            "\x83\xAB\xAB" +
            "\xAB" +
            "\xE8\xAB\xAB\xFF\xFF" +
            "\x83\xAB\xAB" +
            "\xC7\x45\xAB\xFF\xFF\xFF\xFF")
        addrOffset = 10
        self.hookAddrOffset = 12
        offset = self.exe.codeWildcard(code,
                                       "\xAB",
                                       self.hookAddr2,
                                       self.hookAddr2 + 100)
    if offset is False:
        self.exe.log("failed in seach hook addr")
        self.showRawAddr("hookAddr2", self.hookAddr2)
        exit(1)
        return False
    self.hookAddr = self.getAddr(offset, addrOffset, addrOffset + 4)
