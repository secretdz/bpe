#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchCheatDefender(self, errorExit):
    offset, section = self.exe.string("CDClient.dll")
    if offset is False:
        self.exe.log("Cheat defender not found")
        return False

    # 2018-03-15
    # 0  mov ecx, g_CCheatDefenderMgr
    # 6  call CCheatDefenderMgr_init
    # 11 cmp al, 1
    # 13 mov eax, g_CCheatDefenderMgr
    # 18 setz cl
    # 21 push offset string_buffer
    # 26 mov [eax+5], cl
    # 29 call CRagConnection_instanceR
    # 34 mov ecx, eax
    # 36 call CRagConnection_some_func
    code = (
        "\x8B\x0D\xAB\xAB\xAB\x00" +
        "\xE8\xAB\xAB\xAB\xFF" +
        "\x3C\x01" +
        "\xA1\xAB\xAB\xAB\xAB" +
        "\x0F\x94\xC1" +
        "\x68\xAB\xAB\xAB\x01" +
        "\x88\x48\x05" +
        "\xE8\xAB\xAB\xAB\xFF" +
        "\x8B\xC8" +
        "\xE8")
    cheatDefenderMgrOffset1 = 2
    cheatDefenderMgrOffset2 = 14
    cheatDefenderMgrInitOffset = 7
    instanceOffset = 30
    offsets = self.exe.codesWildcard(code, "\xAB", -1)
    if offsets is False:
        # 2018-03-15
        # 0  mov ecx, g_CCheatDefenderMgr
        # 6  call CCheatDefenderMgr_init
        # 11 cmp al, 1
        # 13 mov eax, g_CCheatDefenderMgr
        # 18 setz cl
        # 21 push offset string_buffer
        # 26 mov [eax+5], cl
        # 29 call CRagConnection_instanceR
        # 34 mov ecx, eax
        # 36 call CRagConnection_some_func
        code = (
            "\x8B\x0D\xAB\xAB\xAB\x00" +
            "\xE8\xAB\xAB\xAB\xFF" +
            "\x3C\x01" +
            "\xA1\xAB\xAB\xAB\xAB" +
            "\x0F\x94\xC1" +
            "\x68\xAB\xAB\xAB\x00" +
            "\x88\x48\x05" +
            "\xE8\xAB\xAB\xAB\xFF" +
            "\x8B\xC8" +
            "\xE8")
        cheatDefenderMgrOffset1 = 2
        cheatDefenderMgrOffset2 = 14
        cheatDefenderMgrInitOffset = 7
        instanceOffset = 30
        offsets = self.exe.codesWildcard(code, "\xAB", -1)
    if offsets is False or len(offsets) != 2:
        self.exe.log("Error: cheat defender block not found")
        if errorExit is True:
            exit(1)
        return False

    for offset in offsets:
        cheatDefenderMgr1 = self.exe.readUInt(offset + cheatDefenderMgrOffset1)
        cheatDefenderMgr2 = self.exe.readUInt(offset + cheatDefenderMgrOffset2)
        if cheatDefenderMgr1 != cheatDefenderMgr2:
            self.exe.log("Error: found different cheat defender instances")
            exit(1)
        if self.gCheatDefenderMgr != 0 and \
           self.gCheatDefenderMgr != cheatDefenderMgr1:
            self.exe.log("Error: found different cheat defender values")
            exit(1)
        self.gCheatDefenderMgr = cheatDefenderMgr1
        instance = self.getAddr(offset, instanceOffset, instanceOffset + 4)
        if self.instanceR != 0:
            if instance != self.instanceR:
                self.exe.log("Error: detected wrong CRagConnection_instanceR.")
                exit(1)
        else:
            self.instanceR = instance
            self.addRawFunc("CRagConnection::instanceR", self.instanceR)
        cheatDefenderMgrInit = self.getAddr(offset,
                                            cheatDefenderMgrInitOffset,
                                            cheatDefenderMgrInitOffset + 4)
        if self.cheatDefenderMgrInit != 0 and \
           self.cheatDefenderMgrInit != cheatDefenderMgrInit:
            self.exe.log("Error: found different cheat defender inits")
            exit(1)
        self.cheatDefenderMgrInit = cheatDefenderMgrInit

    self.addVaVar("g_CCheatDefenderMgr", self.gCheatDefenderMgr)
    self.addRawFunc("CCheatDefenderMgr::init",
                    self.cheatDefenderMgrInit)

    # 2018-03-15
    # 0 mov eax, g_CCheatDefenderMgr
    # 5 mov byte ptr [eax+5], 1
    # 9 mov eax, CZ_ENTER
    code = (
        "\xA1\xAB\xAB\xAB\x00" +
        "\xC6\x40\x05\x01" +
        "\xB8\xAB\xAB\x00\x00")
    cheatDefenderMgrOffset = 1
    shuffleOffset = 10
    offset = self.exe.codeWildcard(code,
                                   "\xAB",
                                   offsets[1],
                                   offsets[1] + 0x150)
    if offset is False:
        self.exe.log("Error: cheat defender block 2 not found")
        if errorExit is True:
            exit(1)
        return False
    cheatDefenderMgr1 = self.exe.readUInt(offset + cheatDefenderMgrOffset)
    if cheatDefenderMgr1 != self.gCheatDefenderMgr:
        self.exe.log("Error: found different cheat defender in block 2")
        exit(1)
    self.shuffle23 = self.exe.readUInt(offset + shuffleOffset)
    self.exe.log("Shuffle packet 23 (CZ_ENTER): {0}".format(
        hex(self.shuffle23)))
    return True
