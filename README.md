Batch packet and information extractor for kro and other clients (bpe).

CI status: [![build status](https://gitlab.com/4144/bpe/badges/master/build.svg)](https://gitlab.com/4144/bpe/commits/master)

Forum topic: [http://herc.ws/board/topic/15589-batch-packets-client-info-extractor](http://herc.ws/board/topic/15589-batch-packets-client-info-extractor/)

Bug tracker: [https://gitlab.com/4144/bpe/issues](https://gitlab.com/4144/bpe/issues)

License: GPL-3

# Basic info

Clients what should be processed, need put into clients directory.

Each script can be run for one client or in batch mode.

## One client example

``./info.py CLIENTNAME.exe``

Will run info.py only for given exe name.

## Batch mode example

Run ``./info.py``

Will run info.py for any client located in clients directory.


# Extract packets:

Supported client versions from 2003-10-28 to 2012-07-02 with some exceptions.

Run ``./packets.py``

# Extract information:

This mode can extract information mostly from new clients and partially from any old clients.

Run ``./info.py``

# Create packets table hook script:

This mode can extract information mostly from new clients.

Run ``./olly.py``

# Create configuration file for packet logger:

This mode allow create configuration and header file for packet logger.

Run ``./packetlogger.py``

# Extract max characters to create allowed by client:

This mode extract maximum possible number of chars in client.

Run ``./maxchars.py``

# Extract classes list:

This mode extract all class names present in client with basic classes.

Run ``./class.py``

# Extract messages string table:

This mode extract all messages from messages string table.

Run ``./msgstringtable.py``

